//
//  chatViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 1/29/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth

class chatViewController: UIViewController{

    var firstMess = [Message]()
    var finalMessages = [Message]()
    var receiverName:String?
    var myID:String?
    var myName:String?
    var receiverID:String?
    var offerTitle:String?
    var postTitles = [String]()
    var senderId:String?
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var inputViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var messageBox: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        self.navigationController?.isToolbarHidden = true
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "AmericanTypewriter", size: 25)!]
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.senderId = myID
        self.grabMessages()
        print("finalMessages:", self.finalMessages)
        self.tableView.separatorColor = UIColor.clear
        dismissKeyboard()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tableView.reloadData()
            if self.finalMessages.count > 0 {
                let ind = IndexPath(row: self.finalMessages.count - 1, section: 0)
                self.tableView.scrollToRow(at: ind, at: .bottom, animated: true)
            }
        }
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.reloadData()
        
    }
    
    func dismissKeyboard(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(done))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func done(){
        self.view.endEditing(true)
        self.tableView.reloadData()
    }
    
    @IBAction func sent(_ sender: Any) {

        
        let text = self.messageBox.text
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.messageBox.text = ""
        }
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages")
        let currentDate = Date().timeIntervalSinceReferenceDate
        let seconds = Double(currentDate)
        let timestamp = NSDate(timeIntervalSince1970: seconds)
        let dateformat = DateFormatter()
        dateformat.dateFormat = "hh:mm a"
        dateformat.string(from: timestamp as Date)
        let message = ["sender_id":senderId, "sender_name":self.myName!, "text":text!, "time":String(describing:currentDate), "receiverID": self.receiverID, "title" : self.offerTitle]
        ref.child("\(self.offerTitle!)").child("\(self.senderId!)to\(self.receiverID!)").childByAutoId().setValue(message)
        ref.child("\(self.offerTitle!)").child("\(self.senderId!)to\(self.receiverID!)").updateChildValues(["\(self.receiverID!)-read":"no"])
        
        self.finalMessages.append(Message(sender: self.senderId!, receiver: self.receiverID!, timeStamp: String(describing:currentDate), text: text!))
        let ind = IndexPath(row: self.finalMessages.count - 1, section: 0)
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: [ind], with: .bottom)
        self.tableView.endUpdates()
        self.tableView.setNeedsLayout()
        
    }
    
  
    
    func organizeMess(){
       
         
            var timeStamps = [String]()
            for mess in 0..<self.firstMess.count{
                timeStamps.append(self.firstMess[mess].timeStamp)
            }
            if timeStamps.count == self.firstMess.count{
                timeStamps.sort {
                    $0 < $1
                }
                
             for i in 0..<timeStamps.count {
                    for m in 0..<self.firstMess.count{
                        if self.firstMess[m].timeStamp == timeStamps[i]{
                         let receiver = self.firstMess[m].receiver
                            let id = self.firstMess[m].sender
                            let name = ""
                            let text = self.firstMess[m].text
                            let seconds = Double(self.firstMess[m].timeStamp)
                            let date = NSDate(timeIntervalSince1970: seconds!)
                            let dateformat = DateFormatter()
                            dateformat.dateFormat = "hh:mm a"
                            dateformat.string(from: date as Date)
                         self.finalMessages.append(Message(sender: id, receiver: receiver, timeStamp: dateformat.string(from: date as Date), text: text))
                        }
                    }
                }
            }
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: .UIKeyboardWillHide, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow , object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide , object: nil)
    }

    @objc func keyboardWillAppear(notification: NSNotification?) {

        guard let keyboardFrame = notification?.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }

        let keyboardHeight: CGFloat
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }

        inputViewBottom.constant = keyboardHeight + 8
    }

    @objc func keyboardWillDisappear(notification: NSNotification?) {
        inputViewBottom.constant = 8
    }
     
    
    
  func grabMessages(){
          //querying for the messages
    
          let query = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages").child("\(self.offerTitle!)").child("\(self.senderId!)to\(self.receiverID!)")
          query.queryOrdered(byChild: "time").observe(.value) { (snapshot) in
              if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                  for child in snapshots{
                      let data = child.value as? NSDictionary
                      let id = data?["sender_id"] as? String ?? ""
                      //let name = data?["sender_name"] as? String ?? ""
                      let text = data?["text"] as? String ?? ""
                      let receiver = data?["receiverID"] as? String ?? ""
                      let time = data?["time"] as? String ?? ""
                    
                      if id == self.myID || receiver == self.myID{
                          let message = Message(sender: id, receiver: receiver, timeStamp: time, text: text)
                          self.firstMess.append(message)
                        
                      }
                  }
              }
          }
          let cast = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages").child("\(self.offerTitle!)").child("\(self.receiverID!)to\(self.senderId!)")
          cast.queryOrdered(byChild: "time").observe(.value) { (snapshot) in
              if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                  for child in snapshots{
                      let data = child.value as? NSDictionary
                      let id = data?["sender_id"] as? String ?? ""
                      //let name = data?["sender_name"] as? String ?? ""
                      let text = data?["text"] as? String ?? ""
                      let receiver = data?["receiverID"] as? String ?? ""
                      let time = data?["time"] as? String ?? ""
                   
                      if id == self.myID || receiver == self.myID{
                          let message = Message(sender: id, receiver: receiver, timeStamp: time, text: text)
                          self.firstMess.append(message)
                            
                      }
                  }
              }
          }
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
              self.organizeMess()
          }
      }
    
    

}
extension chatViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("rows",self.finalMessages.count)
        return self.finalMessages.count
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
           let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
           label.numberOfLines = 0
           label.lineBreakMode = NSLineBreakMode.byWordWrapping
           label.font = font
           label.text = text

           label.sizeToFit()
           return label.frame.height
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return heightForView(text: self.finalMessages[indexPath.row].text, font: UIFont.systemFont(ofSize: 17), width: 150) + 30
    }
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! messageTableViewCell
        
        let message = self.finalMessages[indexPath.row]
        cell.textMess = message.text
        
        if message.sender == self.myID {
            cell.mine = true
        }else{
            cell.mine = false
        }
        
        return cell
    }
    
    
}

