//
//  popUpViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/30/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class popUpViewController: UIViewController, UITextFieldDelegate {

   
   
    @IBOutlet weak var priceCheck: UIImageView!
    
    @IBOutlet weak var descripCheck: UIImageView!
    @IBOutlet weak var titleCheck: UIImageView!
    @IBOutlet weak var titleBox: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var descriptionBox: UITextField!
    
    @IBOutlet weak var priceBox: UITextField!
    

    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var addButton: UIButton!
    var ref:DatabaseReference?
    var picker = UIImagePickerController()
    var ownUserName = [String]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isToolbarHidden = false
        
//        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        self.showAnimate()
        self.navigationController?.isToolbarHidden = false
        titleBox.delegate = self
        descriptionBox.delegate = self
        priceBox.delegate = self
        picker.delegate = self
        self.navigationItem.title = "New Listing"
//        self.detailsBox.layer.cornerRadius = 10
//        self.detailsBox.backgroundColor = UIColor.white.withAlphaComponent(0.75)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.view.addGestureRecognizer(tap)
        self.imageView.addGestureRecognizer(tap)
        self.descriptionBox.attributedPlaceholder = NSAttributedString(string: "quick description", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        self.priceBox.attributedPlaceholder = NSAttributedString(string: "price", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        self.titleBox.attributedPlaceholder = NSAttributedString(string: "title", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        addDoneButton()
        if #available(iOS 13.0, *) {
            addToolbarItems()
        } else {
            // Fallback on earlier versions
        }
    }
    
    @available(iOS 13.0, *)
    func addToolbarItems(){
        let dismiss = UIBarButtonItem(title: "cancel", style: .plain, target: self, action: #selector(dismissPressed(_:)))
        
        let next = UIBarButtonItem(title: "preview", style: .plain, target: self, action: #selector(previewPressed))
        let middleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        
        self.toolbarItems = [dismiss, middleSpace, next]
    }
    
    func addDoneButton(){
        let keyboardToolBar = UIToolbar()
        keyboardToolBar.sizeToFit()
         
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked) )
         
        keyboardToolBar.setItems([flexibleSpace, doneButton], animated: true)
         
        descriptionBox.inputAccessoryView = keyboardToolBar
        titleBox.inputAccessoryView = keyboardToolBar
        priceBox.inputAccessoryView = keyboardToolBar
    }
    

    @objc func doneClicked(){
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func priceValidate(_ sender: Any) {
        if (self.priceBox.text?.count)! > 0{
            self.priceCheck.isHidden = false
            
        }else{
            self.priceCheck.isHidden = true
        }
    }
    
    
    @IBAction func descripValidate(_ sender: Any) {
        if (self.descriptionBox.text?.count)! > 10{
            self.descripCheck.isHidden = false
        }else{
            self.descripCheck.isHidden = true
        }
    }
    
    @IBAction func titleValidate(_ sender: Any) {
        if (self.titleBox.text?.count)! > 3{
            self.titleCheck.isHidden = false
        }else{
            self.titleCheck.isHidden = true
        }
    }
    
    @objc func tapped(sender: UIGestureRecognizer) {
        self.view.endEditing(true)
        print("tapped")
    }
    
    @IBAction func uploadPressed(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo Source", preferredStyle: UIAlertControllerStyle.actionSheet)
        let photoLibrary = UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) -> Void in
            print("from library")
            self.uploadButton.isHidden = true
            self.present(self.picker, animated: true, completion: nil)
        })
        let cameraOption = UIAlertAction(title: "Take a photo", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) -> Void in
            print("take a photo")
            //show camera
            self.picker.allowsEditing = true
            self.picker.sourceType = .camera
            self.picker.modalPresentationStyle = .popover
            self.uploadButton.isHidden = true
            self.present(self.picker, animated: true, completion: nil)
        })
        let cancelOption = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) -> Void in
            print("Cancel")
            self.dismiss(animated: true, completion: nil)
        })
        optionMenu.addAction(photoLibrary)
        optionMenu.addAction(cancelOption)
        if UIImagePickerController.isSourceTypeAvailable(.camera) == true{
            optionMenu.addAction(cameraOption)
        }else{
            print("I dont have a camera")
        }
        
        present(optionMenu, animated: true, completion: nil)
    }
    
   
    @objc func previewPressed(_sender:Any){
        if self.titleCheck.isHidden == true && self.priceCheck.isHidden == true && self.descripCheck.isHidden == true {
            let alertController = UIAlertController(title: "Oops", message: "Please fill out all fields", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }else{
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "previewSB") as! previewViewController
            let price1 = getFormattedCurrency(number: Int(self.priceBox.text!)!)
        
            let post = tempPost(descrip: self.descriptionBox.text!, title: self.titleBox.text!, price: price1, image: self.imageView.image!, userName: self.ownUserName[0])
            vc.post = post
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
  
    
    
    func getFormattedCurrency(number: Int) -> String {
      let currencyFormatter = NumberFormatter()
      currencyFormatter.usesGroupingSeparator = true
      currencyFormatter.numberStyle = NumberFormatter.Style.currency
      
      currencyFormatter.locale = NSLocale.current
      let priceString = currencyFormatter.string(from: number as NSNumber)
      return priceString!
        
    }

    
  
    
    @objc func dismissPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "backToHome", sender: self)
    }
    
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func dismissAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished) in
            if (finished){
                self.view.removeFromSuperview()
            }
        }
    }
    
    
    
}
extension popUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            self.imageView.image = pickedImage
            self.imageView.contentMode = .scaleAspectFill
            self.imageView.isHidden = false
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        self.uploadButton.isHidden = false
        
    }
    
    
    
}
