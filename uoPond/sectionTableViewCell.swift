//
//  sectionTableViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 2/4/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class sectionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    

    @IBOutlet weak var senderButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var posts = [sectionPost]()
    @IBOutlet weak var categoryLabel: UILabel!
    var correctPosts = [sectionPost]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
//        collectionView.tag = row
        collectionView.reloadData()
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: 194, height: 161)
        flowLayout.scrollDirection = .horizontal
//        flowLayout.minimumLineSpacing = 5.0
        flowLayout.minimumInteritemSpacing = 10.0
        self.collectionView.collectionViewLayout = flowLayout
        DispatchQueue.global(qos: .background).async {
            self.gatherPosts(completion: { () -> Void in
                self.collectionView.reloadData()
                self.organizePosts()
            })
        }
    }
    
   
    
    
      func gatherPosts(completion: @escaping () -> Void?){
            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
            ref.observe(.value) { (snapshot) in
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                    for child in snapshots{
                        let value = child.value as? NSDictionary
                        let category = value?["section"] as? String ?? ""
                        let downloadURL = value?["image"] as? String ?? ""
                        
                        let post = sectionPost(image: downloadURL, type: category)
                        self.posts += [post]
                    }
                    completion()
                    }
                else{
                    print("ERROR")
                }
                }
        }

    func organizePosts(){
        for post in 0..<self.posts.count {
            if self.posts[post].type == self.categoryLabel.text {
                self.correctPosts.append(self.posts[post])
            }
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsetsMake(10, 0, 0, 0)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.correctPosts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionItem", for: indexPath) as! sectionCollectionViewCell
       
        let downloadURL = self.posts[indexPath.row].image
        let storageRef = Storage.storage().reference(forURL: downloadURL)
        storageRef.getData(maxSize: 20000000, completion: { (data, error) in
            if error != nil{
                print("FIND ME RIGHT HERE:", error!)
                return
            }else{
                let postImage = UIImage(data: data!)
                cell.imagePost.image = postImage
            }
        })
        
        
        return cell
    }
    
    

}
