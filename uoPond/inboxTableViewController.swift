//
//  inboxTableViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 7/13/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class inboxTableViewController: UITableViewController {

  
    
    var mess = [Message]()
    var userTitle:String?
    var myID:String?
    var toID:String?
    var profImages = [UIImage]()
    var profNames = [String]()
    var myName:String?
    var profText = [String]()
    var finalRows:Int = 0
    var myMess = [Message]()
    var displayMess = [Message]()
    var finalMess = [Message]()
    var messages = [Message]()
    var offerTitles = [String]()
    var postTitles = [String]()
    var ifRead = [String]()
    var Rows = 1
    var profile = [String]()
    
    
    @objc func refresh(sender:AnyObject){
        self.tableView.reloadData()
        self.refreshControl?.endRefreshing()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.title = "Inbox"
        self.refreshControl?.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
        
        //self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "AmericanTypewriter", size: 25)!]
        self.navigationController?.navigationBar.backgroundColor = UIColor.lightGray
        self.navigationController?.navigationBar.barTintColor = UIColor.lightGray
        
        self.tableView.reloadData()
    }

   
    override func viewDidDisappear(_ animated: Bool) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "homeSB") as! homeViewController
        for title in VC.myTitles{
            VC.gatherMessages(title: title)
        }
        //VC.viewDidLoad()
    }
  
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("NUMBER OF ROWS:",self.Rows)
        if Rows == 0 {
            self.Rows = 1
        }
        return Rows
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "contactSB") as! chatViewController
        //let cell = tableView.dequeueReusableCell(withIdentifier: "inboxCell", for: indexPath) as! inboxTableViewCell
        VC.myID = self.myID
        if self.myID == self.finalMess[indexPath.row].receiver{
            guard self.finalMess[indexPath.row].sender == self.profile[indexPath.row] else{
                print("error in guard")
                self.navigationController?.popViewController(animated: true)
                return
            }
            VC.receiverID = self.finalMess[indexPath.row].sender
        }else if self.myID == self.finalMess[indexPath.row].sender{
            guard self.finalMess[indexPath.row].receiver == self.profile[indexPath.row] else{
                print("error in guard 2")
                self.navigationController?.popViewController(animated: true)
                return
            }
            VC.receiverID = self.finalMess[indexPath.row].receiver
        }
        VC.postTitles = self.postTitles
        VC.navigationItem.title = self.profNames[indexPath.row]
        VC.receiverName = self.profNames[indexPath.row]
        VC.offerTitle = self.offerTitles[indexPath.row]
        //print("OFFER TITLE : MAY BE PROBLEM: ",self.offerTitles[indexPath.row])
        VC.myName = self.myName
        let cell = tableView.dequeueReusableCell(withIdentifier: "inboxCell", for: indexPath) as! inboxTableViewCell
        cell.readLabel.isHidden = true
        let ref = Database.database().reference().child("Messages").child(self.offerTitles[indexPath.row])
        ref.observe(.value) { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
            for child in snapshots{
                let key = child.key
                if key.contains("to\(self.myID!)") {
                    let cast = Database.database().reference().child("Messages").child(self.offerTitles[indexPath.row]).child(key)
                    cast.observe(.value) { (snap) in
                        if let snapshots = snap.children.allObjects as? [DataSnapshot]{
                            for child in snapshots{
                                if child.key == "\(self.myID!)-read"{
                                    cast.child(child.key).setValue("yes")
                                    self.ifRead[indexPath.row] = "yes"
                                }
                            }
                        
                        }
                    }
                }
                }
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "newMessage"{
//            let vc = segue.destination as! UINavigationController
//            let target = vc.topViewController as! messagingViewController
//            let path = tableView.indexPathForSelectedRow
//            target.receiverName = self.profNames[path!.row]
//            target.navigationController?.title = self.profNames[(path?.row)!]
//            target.myID = self.myID
//            if self.mess[(path?.row)!].sender == self.myID{
//                target.receiverID = self.mess[(path?.row)!].receiver
//            }else {
//                target.receiverID = self.mess[(path?.row)!].sender
//            }
//        }
//    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "inboxCell", for: indexPath) as! inboxTableViewCell
        var profile:String?
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            print("IN OVERRIDE :", self.Rows)
            if self.displayMess.count == 0{
                cell.userName.text = "No Messages Yet"
                cell.latestText.font = UIFont.systemFont(ofSize: 15)
                cell.timeLabel.isHidden = true
                cell.userImage.isHidden = true
            }else if self.displayMess.count > 0{
                if self.myID == self.displayMess[indexPath.row].receiver{
                    profile = self.displayMess[indexPath.row].sender
                    print("PROFILE:",profile!)
                }else{
                    profile = self.displayMess[indexPath.row].receiver
                    print("PROFILE:",profile!)
                }
                self.profile.append(profile!)
                let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
                let userRef = ref.child("/\(profile!)")
                cell.userImage.isHidden = false
                userRef.observe(.value, with: { (snapshot) in
                    if let dictionary = snapshot.value as? [String:String]{
                        let userName = dictionary["username"]
                        self.profNames.append(userName!)
                        cell.latestText.text = "\"\(self.displayMess[indexPath.row].text)\"..."
                        cell.userName.text = userName
                        cell.offerTitle.text = self.offerTitles[indexPath.row]
                        print(self.ifRead[indexPath.row])
                        if self.ifRead[indexPath.row] == "no" {
                            cell.readLabel.isHidden = false
                        }
                        let profPicRef = Storage.storage().reference(withPath: "/users/\(profile!)/profPicture")
                        profPicRef.getData(maxSize: 200000000) { (data, err) in
                            if err != nil {
                                print("uh oh error occured (prob exceeded limit)")
                            }else{
                                cell.userImage.image = UIImage(data: data!)
                            }
                        }
                        
                        
                        if let seconds = Double(String(self.displayMess[indexPath.row].timeStamp) ){
                            let timestamp = NSDate(timeIntervalSince1970: seconds)
                            let dateformat = DateFormatter()
                            dateformat.dateFormat = "hh:mm a"
                            cell.timeLabel.isHidden = false
                            cell.timeLabel.text = dateformat.string(from: timestamp as Date)
                        }
                    }
                }, withCancel: nil)
            }
        }
        return cell
    }
    


}
