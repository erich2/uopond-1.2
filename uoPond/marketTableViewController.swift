//
//  marketTableViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 5/21/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class marketTableViewController: UITableViewController, UISearchBarDelegate {
    
   
    
    
    @IBOutlet weak var searchBAR: UISearchBar!
    
    var marketPosts = [Post]()
    var marketImage = [UIImage]()
    //typealias functionFinished = () -> ()
    var refresh: UIRefreshControl!
    var isSearching = false
    var searchPost = [Post]()
    var userTitle = [String]()
    var uid = [String]()
    var myID:String?
    var myName:String?
    var profImage:UIImage?
    var clickedPath: IndexPath?
    var venmo = [String]()
    var descripHeights = [CGFloat]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.backgroundColor = UIColor.lightGray
        self.navigationController?.navigationBar.barTintColor = UIColor.lightGray
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.isToolbarHidden = false
        //self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tableView.backgroundColor = UIColor.white
        searchBAR.delegate = self
        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor.black
        
        tableView.addSubview(refresh)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
        
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "AmericanTypewriter", size: 25)!]
        //self.navigationController?.navigationBar = naviBar
        
        tableView.delegate = self
        DispatchQueue.global(qos: .background).async {
            self.gatherPosts(completion: { () -> Void in
                for u in 0..<self.marketPosts.count{
                    let user = self.marketPosts[u].user
                    self.addVenmos(user: user)
                    print("venmos:", self.venmo)
                }
                self.tableView.reloadData()
            })
        }
        addDoneButton()
        print("Did load:",self.marketPosts.count)
        self.searchBAR.frame.size.height = 0
        
        getToolbar()
    }

 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = false
    }
    
    
    func getToolbar(){
        if #available(iOS 13.0, *) {
            let searchBarButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .plain, target: self, action: #selector(searchTapped(_:)))
//            let profile = UIBarButtonItem(image: UIImage(systemName: "person.circle.fill"), style: .plain, target: self, action: #selector(profButtonPressed(_:)))
            
            let rightSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
//            let leftSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            self.toolbarItems = [  rightSpace, searchBarButton]
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    func addDoneButton(){
        let keyboardToolBar = UIToolbar()
        keyboardToolBar.sizeToFit()
         
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked) )
         
        keyboardToolBar.setItems([flexibleSpace, doneButton], animated: true)
         
        searchBAR.inputAccessoryView = keyboardToolBar
    }
    
    @objc func doneClicked(){
        view.endEditing(true)
    }

    
    
    @objc func profButtonPressed(_ sender: Any) {
        //self.performSegue(withIdentifier: "toProf", sender: self)
        /*let VC = self.storyboard!.instantiateViewController(withIdentifier: "homeSB") as! homeViewController
        VC.userUid = self.myID
        VC.gatherMyTitles()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            for title in VC.myTitles{
                VC.gatherMessages(title: title)
            }
        }*/
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @objc func searchTapped(_ sender: Any) {
        if searchBAR.frame.size.height == 0{
            UIView.animate(withDuration: 0.5) {
                self.searchBAR.frame.size.height = 44
                self.tableView.reloadData()
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                self.searchBAR.frame.size.height  = 0
                self.tableView.reloadData()
            }
        }
    }
    
    func addVenmos(user:String){
        let cat = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
        cat.observe(.value) { (snapshot) in
            if let snaps = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snaps{
                    if child.key == user{
                        let value = child.value as? NSDictionary
                        let vmo = value?["venmo"] as? String ?? ""
                        self.venmo.append("@\(vmo)")
                    }
                }
            }
        }
    }
    
    
    func gatherPosts(completion: @escaping () -> Void?){
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
        ref.observe(.value) { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snapshots{
                    let value = child.value as? NSDictionary
                    
                    let section = value?["section"] as? String ?? ""
                    if section != nil && section == self.navigationItem.title {
                        let user = value?["user"] as? String ?? ""
                                           let title = value?["title"] as? String ?? ""
                                           let descrip = value?["description"] as? String ?? ""
                                           let price = value?["price"] as? String ?? ""
                                           let downloadURL = value?["image"] as? String ?? ""
                                           let userName = value?["userName"] as? String ?? ""
                                           let postID = value?["postID"] as? String ?? ""
                                           let marketPost = Post(descrip: descrip, title: title, price: price, imageURL: downloadURL, user: user, userName: userName, postID: postID)
                                           self.marketPosts += [marketPost]
                    }

                }
                completion()
                }
            else{
                print("ERROR")
            }
            }
        
        print("after function:",self.marketPosts.count)
    }
    
    
    @IBAction func toHome(_ sender: Any) {
        self.performSegue(withIdentifier: "marketToHome", sender: self)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isSearching{
            return searchPost.count
        }else{
            return marketPosts.count
        }
    }
    
    // MARK: - search bar

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            tableView.endEditing(true)
            tableView.reloadData()
        }else{
            isSearching = true
            print("searching......")
            searchPost = marketPosts.filter({$0.title.uppercased().contains(searchText.uppercased())} )
            tableView.reloadData()
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var device: Post!
        if isSearching {
            device = self.searchPost[indexPath.row]
        }else{
            device = self.marketPosts[indexPath.row]
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "marketCell", for: indexPath) as! marketTableViewCell
        let downloadURL = device.imageURL
        let storageRef = Storage.storage().reference(forURL: downloadURL)
        storageRef.getData(maxSize: 20000000, completion: { (data, error) in
            if error != nil{
                print("FIND ME RIGHT HERE:", error!)
                return
            }else{
                let postImage = UIImage(data: data!)
                cell.postImage.image = postImage
            }
        })
        let user = device.user
        let profPicRef = Storage.storage().reference(withPath: "/users/\(user)/profPicture")
        profPicRef.getData(maxSize: 200000000) { (data, err) in
            if err != nil {
                print("uh oh error occured")
            }else{
                let userImage = UIImage(data:data!)
                cell.userImage.image = userImage!
            }
        }
        cell.descripBox.text = device.descrip
        
        cell.titleLabel.text = device.title
        cell.priceLabel.text = device.price
        
        self.userTitle = [device.user]
        cell.messageButton.tag = indexPath.row
        cell.messageButton.addTarget(self, action: #selector(message), for: .touchUpInside)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toProf"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! homeViewController
            target.userUid = self.myID!
        }
    }
    
    @objc func message(sender:Any?){
        let button = sender as! UIButton
        let path = button.tag
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "contactSB") as! chatViewController
        print("THE PATH:",path)
        VC.myName = self.myName
        VC.myID = self.myID
        VC.receiverName = self.marketPosts[(path)].userName
        print("USERNAME:",self.marketPosts[path].userName)
        VC.offerTitle = self.marketPosts[(path)].title
        VC.receiverID = self.marketPosts[(path)].user
        VC.navigationItem.title = self.marketPosts[(path)].userName
        
        if self.venmo[path] != nil{
             VC.navigationItem.prompt = self.venmo[path]
        }
       
        self.navigationController?.pushViewController(VC, animated: true)
        
    }
    
 /*   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let path = tableView.indexPathForSelectedRow
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "chatSB") as! messagingViewController
        self.navigationController?.pushViewController(VC, animated: true)
        VC.myName = self.myName
        VC.myID = self.myID
        VC.receiverName = self.marketPosts[(path?.row)!].userName
        VC.offerTitle = self.marketPosts[(path?.row)!].title
        VC.receiverID = self.marketPosts[(path?.row)!].user
        VC.navigationItem.title = self.marketPosts[(path?.row)!].userName
    }
    */
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

