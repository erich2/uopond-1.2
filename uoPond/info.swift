//
//  info.swift
//  uoPond
//
//  Created by Ethan Richards on 4/30/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import Foundation
import UIKit

struct Post{
    var descrip:String
    var title:String
    var price:String
    var imageURL:String
    var user:String
    var userName:String
    var postID:String
}

struct tempPost{
    var descrip:String
    var title:String
    var price:String
    var image:UIImage
    var userName:String
}


struct sectionPost {
    var image:String
    var type:String
}
