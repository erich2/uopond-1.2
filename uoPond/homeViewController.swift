//
//  homeViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/22/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth


class homeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var settingsTab: UIBarButtonItem!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var venmoUserName: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var inboxButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var trashCan: UIButton!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var totalMoney: UILabel!
    @IBOutlet weak var totalItems: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var changePWord: UIButton!
    @IBOutlet weak var changeVenmo: UIButton!
    @IBOutlet weak var venmoField: UITextField!
    @IBOutlet weak var naviView: UIView!
    
    
    var userUid:String?
    var marketPosts = [Post]()
    var marketImage = [UIImage]()
    typealias functionFinished = () -> ()
    var first:String?
    var postTitles = [String]()
    var settingsShowing = false
    var email = [String]()
    var offerTitles = [String]()
    var ifRead = [String]()
    var finalMess = [Message]()
    var displayMess = [Message]()
    var Rows:Int = 0
    var myTitles = [String]()
    var selected = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.userUid = Auth.auth().currentUser?.uid
        self.getEmail()
        self.navigationController?.isToolbarHidden = false
        self.tableView.allowsMultipleSelection = true
        self.tableView.allowsMultipleSelectionDuringEditing = true
        self.settingsView.isHidden = true
        logoutButton.layer.cornerRadius = 10
        //self.naviView.backgroundColor = UIColor.lightGray
        self.naviView.layer.borderColor = UIColor.lightGray.cgColor
        self.naviView.layer.borderWidth = 1
        self.totalMoney.adjustsFontSizeToFitWidth = true
        self.totalItems.adjustsFontSizeToFitWidth = true
        logoutButton.layer.borderWidth = 1
        logoutButton.layer.borderColor = UIColor.systemBlue.cgColor
        
        saveButton.layer.cornerRadius = 10
        changeVenmo.layer.cornerRadius = 10
        changePWord.layer.cornerRadius = 10
        saveButton.layer.borderColor = UIColor.systemBlue.cgColor
        saveButton.layer.borderWidth = 1
        changeVenmo.layer.borderColor = UIColor.systemBlue.cgColor
        changeVenmo.layer.borderWidth = 1
       changePWord.layer.borderColor = UIColor.systemBlue.cgColor
        changePWord.backgroundColor = UIColor.white
        changePWord.layer.borderWidth = 1
        self.saveButton.isHidden = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.lightGray
        self.navigationController?.navigationBar.barTintColor = UIColor.lightGray
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "AmericanTypewriter", size: 25)!]
        self.tableView.backgroundColor = UIColor.white
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2
        self.profilePic.clipsToBounds = true
        self.inboxButton.layer.cornerRadius = 10
        
        tableView.delegate = self
        tableView.dataSource = self
        self.venmoField.attributedPlaceholder = NSAttributedString(string: "@venmo..", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        self.navigationItem.title = ""
        self.gatherData(completion: { () -> Void? in
            view.reloadInputViews()
        })
        self.gatherPosts(completion: { () -> Void in
            self.tableView.reloadData()
        })
        self.gatherMyTitles()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            print("in dispatch",self.myTitles)
            for title in self.myTitles{
                self.gatherMessages(title: title)
                print("offer titles:",self.offerTitles)
                self.newMessage(title: title)
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.totalMoney.text = self.getFormattedCurrency(number: self.getTotalCost())
            print("in the total money")
            self.tableView.reloadData()
            for val in 0..<self.selected.count {
                if self.selected[val] == true {
                    self.trashCan.tintColor = UIColor.systemRed
                }
            }
        }
        tableView.rowHeight = 89
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.profilePic.addGestureRecognizer(tap)
        self.profilePic.isUserInteractionEnabled = true
        addDoneButton()

        if #available(iOS 13.0, *) {
            let marketButton = UIBarButtonItem(image: UIImage(systemName: "globe"), style: .plain, target: self, action: #selector(marketPressed))
            let addPost = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(writePost(_:)))
            addPost.setTitleTextAttributes([NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 30)], for: .normal)
            let rightSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            let leftSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            self.toolbarItems = [rightSpace, addPost, leftSpace, marketButton]
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
   
    
    override func viewDidAppear(_ animated: Bool) {
        self.finalMess = []
        self.displayMess = []
        self.offerTitles = []
//        self.marketPosts = []
//        self.marketImage = []
        self.selected = []
        for title in self.myTitles{
            self.gatherMessages(title: title)
            self.newMessage(title: title)
        }
//        self.gatherPosts(completion: {
//            self.tableView.reloadData()
//        })
    }
    
    func addDoneButton(){
        let keyboardToolBar = UIToolbar()
        keyboardToolBar.sizeToFit()
         
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked) )
         
        keyboardToolBar.setItems([flexibleSpace, doneButton], animated: true)
         
        venmoField.inputAccessoryView = keyboardToolBar
    }
    
    @objc func doneClicked(){
        view.endEditing(true)
    }

    @IBAction func settingsClicked(_ sender: Any) {
       
            if #available(iOS 13.0, *) {
                if self.settingsView.isHidden == true {
                           self.settingsView.isHidden = false
                            self.settingsTab.image = UIImage(systemName: "chevron.up")
                }else{
                    self.settingsView.isHidden = true
                    self.settingsTab.image = UIImage(systemName: "chevron.down")
                }
            } else {
                // Fallback on earlier versions
            }
        
    }
    
    @IBAction func trashCanRows(_ sender: Any) {
        
        if self.trashCan.tintColor == UIColor.systemRed{
            for val in 0..<self.selected.count {
                print(self.selected)
                if self.selected[val] == true{
                    print(val, "--> index")
                    let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/").child("Posts")
                    ref.child("Post\(self.marketPosts[val].postID)").removeValue()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.marketPosts.remove(at: val)
                        self.selected.remove(at: val)
                        self.tableView.reloadData()
                    }
                    
                   
                }
            }
            self.trashCan.tintColor = UIColor.black
        }
    }
    
    
    @objc func tapped(sender: UIGestureRecognizer){
        let picker = UIImagePickerController()
        self.saveButton.isHidden = false
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func logoutPressed(_ sender: Any) {
        self.showSpinner(onView: self.view)
        self.performSegue(withIdentifier: "signedOut", sender: self)
    }
 
    func gatherMyTitles(){
    let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
    ref.observe(.value) { (snapshot) in
        if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
            for child in snapshots{
                let value = child.value as? NSDictionary
                let title = value?["title"] as? String ?? ""
                self.myTitles.append(title)
                }
            }
        }
    }
    
//    ADD A MIDDLE DESIGN PEICE
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if (self.venmoField.isHidden == false) && (self.venmoField.text != "") {
            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
            let userReference = ref.child(self.userUid!)
            userReference.updateChildValues(["venmo":self.venmoField.text!])
        }
        
        saveButton.isHidden = true
        self.venmoField.isHidden = true
        self.changeVenmo.isHidden = false
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
           
           var selectedImage: UIImage?
           if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
               selectedImage = editedImage
           }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
               selectedImage = originalImage
           }
           self.profilePic.image = selectedImage
           
            guard let image = selectedImage else{
                print("error")
                return
            }
            let imageName = UUID().uuidString
            let data = UIImageJPEGRepresentation(image, 1.0)
            let imageRef = Storage.storage().reference().child("profPics").child("\(imageName).jpg")
            imageRef.putData(data!, metadata: nil) { (metadata, error) in
                if error != nil {
                    print(error!)
                    return
                }else{
                    imageRef.downloadURL(completion: { (url, err) in
                        if err != nil{
                            print(err!)
                            return
                        }else{
                            guard let url = url else{
                                print("unsuccessful")
                                return
                            }
                            let urlString = url.absoluteString
                            let uid = self.userUid!
                            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
                            let userReference = ref.child(uid)
                            let values = ["profilePicture" : urlString]
                            userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                                if err != nil{
                                    print(err!)
                                    return
                                }else{
                                    print("success")
                                }
                            })
                        }
                    })
                }
                
            }
        
           picker.dismiss(animated: true, completion: nil)
       }
       
       
       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        if self.venmoField.text == "" {
            self.saveButton.isHidden = true
        }
           picker.dismiss(animated: true, completion: nil)
       }
    
    
    
    func getEmail(){
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
        let userReference = ref.child(self.userUid!)
        userReference.observe( .value) { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            let userEmail = value?["email"] as? String ?? ""
            self.email = [userEmail]
        }
    }
    

    @IBAction func changePWord(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: self.email[0]) { (err) in
            if err != nil {
                print(err!)
                return
            }else{
                let alertController = UIAlertController(title: "Sent!", message: "Please check your email to reset your password.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func changeVenmo(_ sender: Any) {
        self.saveButton.isHidden = false
        self.changeVenmo.isHidden = true
        self.venmoField.isHidden = false
        
    }
    @IBAction func openInbox(_ sender: Any) {
        
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "inboxSB") as! inboxTableViewController
        self.navigationController?.pushViewController(VC, animated: true)
        VC.myID = self.userUid!
        VC.postTitles = self.myTitles
        VC.myName = self.nameLabel.text
        VC.finalMess = self.finalMess
        VC.displayMess = self.displayMess
        VC.offerTitles = self.offerTitles
        VC.ifRead = self.ifRead
        VC.Rows = self.Rows
    }
    
    func gatherData(completion: () -> Void?){
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
        ref.child("\(self.userUid!)/").observe( .value) { (snapshot) in
                if let value = snapshot.value as? [String:Any] {
            
            //guard let value = snapshot.children.allObjects as! NSDictionary else { print("ERROR"); return }
                    let currentFirst = value["firstName"] as? String ?? ""
                    let currentLast = value["lastName"] as? String ?? ""
                    let vName = value["venmo"] as? String ?? ""
                    self.venmoUserName.text = "@\(vName)"
                    let fullname = currentFirst + " " + currentLast
                    self.nameLabel.text = fullname
                    let username = value["username"] as? String ?? ""
                    self.navigationItem.title = "profile"
                    self.userNameLabel.text = username
                    
                    let profPicRef = Storage.storage().reference(withPath: "/users/\(self.userUid!)/profPicture")
                    profPicRef.getData(maxSize: 200000000) { (data, err) in
                        if err != nil {
                            print("uh oh error occured")
                        }else{
                            self.profilePic.image = UIImage(data: data!)
                        }
                    }
                    
                    
                    
                    
                    /*profPicRef.getMetadata { (metadata, err) in
                        if let err = err {
                            print("uh oh error occured!")
                        } else {
                            self.profilePic = UIImage(data: metadata)
                        }
                    }*/
            //print("USERNAME", username)
            // fix the url saved
                   /* let downloadURL = value["profilePicture"] as? String ?? ""
                    let storageRef = Storage.storage().reference(forURL: downloadURL)
                    storageRef.getData(maxSize: 200000000, completion: { (data, error) in
                        if error != nil{
                            print(error!)
                        
                            return
                        }else{
                            if data == nil {
                        
                            }else{
                                self.profilePic.image = UIImage(data: data!)
                            }
                        }
                })*/
                   
                }else{
                    print("Not creating dictionary")
                }
        }
        completion()
    }

    func newMessage(title:String){
        let query = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages/\(title)")
        query.observe(.value) { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snapshots{
                    let key = child.key
                    if key.contains("\(self.userUid!)") {
                        let cast = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages/\(title)").child(key)
                        cast.observe(.value) { (snap) in
                            if let snapshots = snap.children.allObjects as? [DataSnapshot]{
                                for child in snapshots{
                                    if child.key.contains("-read"){
                                        if child.key == "\(self.userUid!)-read"{
                                            if (child.value as! String) == "no" {
                                                self.inboxButton.titleLabel?.text = "!"
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func gatherMessages(title : String){
             let query = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages/\(title)")
             query.observe(.value) { (snapshot) in
                 if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                     for child in snapshots{
                         let key = child.key
                         if key.contains("\(self.userUid!)") {
                             let cast = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages/\(title)").child(key)
                             cast.observe(.value) { (snap) in
                                 if let snapshots = snap.children.allObjects as? [DataSnapshot]{
                                     for child in snapshots{
                                         if child.key.contains("-read"){
                                             if child.key == "\(self.userUid!)-read"{
                                                 self.ifRead.append(child.value as! String)
                                             }else{
                                                 self.ifRead.append("yes")
                                             }
                                         }else{
                                             let data = child.value as? NSDictionary
                                             let id = data?["sender_id"] as? String ?? ""
                                             let time = data?["time"] as? String ?? ""
                                             let text = data?["text"] as? String ?? ""
                                             let receiver = data?["receiverID"] as? String ?? ""
                                             let offerTitle = data?["title"] as? String ?? ""
                                             if (id == self.userUid!) || (receiver == self.userUid!){
                                                let mess = Message(sender: id, receiver: receiver, timeStamp: time, text: text)
                                                 self.finalMess.append(mess)
                                                 self.offerTitles.append(offerTitle)
                                                
                                                 self.organize()
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
     }
     
     
     func organize(){
           
            print(self.finalMess.count)
            var displayFinalMess = [String:Message]()
                for mess in 0..<self.finalMess.count{
                    if self.finalMess[mess].sender == self.userUid{
                        
                        if displayFinalMess[self.finalMess[mess].receiver] == nil{
                            displayFinalMess[self.finalMess[mess].receiver] = self.finalMess[mess]
                        }else if displayFinalMess[self.finalMess[mess].receiver] != nil{
                            if (displayFinalMess[self.finalMess[mess].receiver]?.timeStamp)! > self.finalMess[mess].timeStamp{
                                break
                            }else{
                                displayFinalMess[self.finalMess[mess].receiver] = self.finalMess[mess]
                            }
                        }
                    }else if self.finalMess[mess].receiver == self.userUid{
                        
                        if displayFinalMess[self.finalMess[mess].sender] == nil{
                            displayFinalMess[self.finalMess[mess].sender] = self.finalMess[mess]
                        }else if displayFinalMess[self.finalMess[mess].sender] != nil{
                            if (displayFinalMess[self.finalMess[mess].sender]?.timeStamp)! > self.finalMess[mess].timeStamp{
                                break
                            }else{
                                displayFinalMess[self.finalMess[mess].sender] = self.finalMess[mess]
                            }
                        }
                    }
                }
            
                self.displayMess = Array(displayFinalMess.values)
             self.Rows = displayFinalMess.count
        }
    
    
  
    @objc func marketPressed(_ sender: Any) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "collectionSB") as! marketBiggerTableViewController
        self.navigationController?.pushViewController(VC, animated: true)
        VC.myID = self.userUid!
        VC.myName = self.nameLabel.text
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMarket"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! marketTableViewController
            target.myID = self.userUid!
            target.myName = self.nameLabel.text
            
        }else if segue.identifier == "signedOut"{
            do{
                try Auth.auth().signOut()
                self.removeSpinner()
            }catch{
                print("error signing out")
                self.removeSpinner()
            }
        }else if segue.identifier == "newPost" {
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! popUpViewController
            target.ownUserName = [(self.navigationItem.title!)]
        }
    }
    
    func gatherPosts(completion: () -> Void?){
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/")
        ref.child("Posts").observe(.value) { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snapshots{
                    let value = child.value as? NSDictionary
                    // if statement making sure user posted it
                    let user = value?["user"] as? String ?? ""
                    if user == self.userUid! {
                        let title = value?["title"] as? String ?? ""
                        let descrip = value?["description"] as? String ?? ""
                        let price = value?["price"] as? String ?? ""
                        let user = value?["user"] as? String ?? ""
                        let downloadURL = value?["image"] as? String ?? ""
                        let userName = value?["userName"] as? String ?? ""
                        let postID = value?["postID"] as? String ?? ""
                        /*
                        let storageRef = Storage.storage().reference(forURL: downloadURL)
                        storageRef.getData(maxSize: 20000000, completion: { (data, error) in
                            if error != nil{
                                print("FIND ME RIGHT HERE:", error!)
                                return
                            }else{
                                let image = UIImage(data: data!)
                                let postImage = image
                                self.tableView.reloadData()
                            }
                        })
                        */
                        let post = Post(descrip: descrip, title: title, price: price, imageURL: downloadURL, user: user, userName: userName, postID: postID)
                        self.marketPosts += [post]
                        self.postTitles += [title]
                        self.selected += [false]
                    }else{
                        print("error")
                    }
                    }
                print("Success")
                print("after success:",self.marketPosts.count)
                self.tableView.reloadData()
            }
        }
        completion()
        //nil right here for some reason
        print("at end of function:", self.marketPosts.count)
    }
 
    
    func getTotalCost() -> Int{
        var total = 0
        for price in 0..<self.marketPosts.count{
            let price = self.marketPosts[price].price
            let amount = Int(price.removeFormatAmount())
            if amount != nil {
                total += amount
            }
        }
        return total
    }
 
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func welcomeUser(){
        let welcome = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "welcomeSB") as! welcomeViewController
        self.addChildViewController(welcome)
        welcome.view.frame = self.view.frame
        self.view.addSubview(welcome.view)
        welcome.didMove(toParentViewController: self)
        
    }
    
    @objc func writePost(_ sender: Any) {
        
        self.performSegue(withIdentifier: "newPost", sender: self)
       
        
    }
    

}
extension homeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func getFormattedCurrency(number: Int) -> String {
        let currencyFormatter = NumberFormatter()
         currencyFormatter.usesGroupingSeparator = true
         currencyFormatter.numberStyle = NumberFormatter.Style.currency
        
         currencyFormatter.locale = NSLocale.current
        let priceString = currencyFormatter.string(from: number as NSNumber)
        return priceString!
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.marketPosts.count > 0{
            if self.selected[indexPath.row] == false {
                self.selected[indexPath.row] = true
                print("selected")
            }else{
                self.selected[indexPath.row] = false
                print("Deselected")
                let cell = self.tableView.cellForRow(at: indexPath)
                cell!.contentView.layer.borderWidth = 0
            }
            for val in 0..<self.selected.count {
                print(self.selected)
                if self.selected[val] == true{
                    self.trashCan.tintColor = UIColor.systemRed
                    return
                }
                self.trashCan.tintColor = UIColor.black
            }
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var result = 1
        if self.marketPosts.count == 0{
            self.totalItems.text = "0 items"
            self.totalMoney.text = self.getFormattedCurrency(number: 0)
            result = 1
        }else{
            if self.marketPosts.count == 1{
                self.totalItems.text = "1 item:"
            }else{
                self.totalItems.text = "\(self.marketPosts.count) items:"
            }
            result = self.marketPosts.count
        }
        return result
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if (editingStyle == .delete) /*&& (self.marketPosts.count > 0) && (self.marketImage.count > 0)*/{
//            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/").child("Posts")
//            ref.child("Post\(self.marketPosts[indexPath.row].postID)").removeValue()
//            self.marketPosts.remove(at: indexPath.row)
//            self.marketImage.remove(at: indexPath.row)
//            self.tableView.deleteRows(at: [indexPath], with: .automatic)
//
//            print("deleted")
//        }
//    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            if (self.marketPosts.count > 0) && (self.marketImage.count > 0) {
//                let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/").child("Posts")
//                ref.child("Post\(self.marketPosts[indexPath.row].postID)").removeValue()
//                self.marketPosts.remove(at: indexPath.row)
//                self.marketImage.remove(at: indexPath.row)
//                self.tableView.reloadData()
                print("index:", indexPath)
            }
        }

        let share = UITableViewRowAction(style: .normal, title: "Disable") { (action, indexPath) in
            // share item at indexPath
        }

        share.backgroundColor = UIColor.blue

        return [delete, share]
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if tableView.isEditing == true{
            return .delete
        }
        return .none
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath) as! homeTableViewCell
        
        if self.marketPosts.count == 0{
            cell.titleLabel.text = "No Posts:("
            cell.priceLabel.isHidden = true
            cell.thumbNail.isHidden = true
            cell.contentView.layer.borderWidth = 0
        }else{
            cell.thumbNail.isHidden = false
            cell.priceLabel.isHidden = false
            cell.titleLabel.text = self.marketPosts[indexPath.row].title
            cell.priceLabel.text = self.marketPosts[indexPath.row].price
            let downloadURL = self.marketPosts[indexPath.row].imageURL
            let storageRef = Storage.storage().reference(forURL: downloadURL)
            storageRef.getData(maxSize: 20000000, completion: { (data, error) in
                if error != nil{
                    print("FIND ME RIGHT HERE:", error!)
                    return
                }else{
                    let postImage = UIImage(data: data!)
                    cell.thumbNail.image = postImage
                    self.tableView.reloadData()
                }
            })
        }
        
        if self.selected.count > 0 {
            if self.selected[indexPath.row] == true {
                cell.contentView.layer.borderColor = UIColor.systemBlue.cgColor
                cell.contentView.layer.borderWidth = 2
            }
        }
        
        return cell
    }
    
}
extension String {
    func removeFormatAmount() -> Double {
        let formatter = NumberFormatter()

        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .currency
        formatter.currencySymbol = "$"
        formatter.decimalSeparator = ","

        return formatter.number(from: self) as! Double? ?? 0
     }
}
