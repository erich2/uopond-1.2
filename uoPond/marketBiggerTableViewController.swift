//
//  marketBiggerTableViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 2/4/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class marketBiggerTableViewController: UITableViewController {

    var data = [String]()
    var refresh: UIRefreshControl!
    var marketPosts = [Post]()
    var marketImage = [UIImage]()
    var userTitle = [String]()
    var uid = [String]()
    var myID:String?
    var myName:String?
    var profImage:UIImage?
    var clickedPath: IndexPath?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.backgroundColor = UIColor.lightGray
        self.navigationController?.navigationBar.barTintColor = UIColor.lightGray
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.isToolbarHidden = false
        // add the logo into the nav bar
//        let logoView = UIImageView(image: UIImage(named: "navImage"))
//
//        logoView.contentMode = .scaleAspectFit
//        self.navigationItem.titleView = logoView
        
        self.data = ["furniture", "notes", "electronics", "clothing", "books"]
        self.tableView.backgroundColor = UIColor.white
        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor.black
        tableView.addSubview(refresh)
        addNavBarImage()
        getToolbar()
    }
    

    func addNavBarImage() {

        let navController = navigationController!

        let image = UIImage(named: "navImage") //Your logo url here
        let imageView = UIImageView(image: image)

        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height

        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2

        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit

        navigationItem.titleView = imageView
    }
    
    
    
    func getToolbar(){
           if #available(iOS 13.0, *) {
               let profile = UIBarButtonItem(image: UIImage(systemName: "person.circle.fill"), style: .plain, target: self, action: #selector(profButtonPressed(_:)))
               //let addPost = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(writePost(_:)))
               //addPost.setTitleTextAttributes([NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 30)], for: .normal)
               let rightSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
               //let leftSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
               self.toolbarItems = [ profile, rightSpace]
           } else {
               // Fallback on earlier versions
           }
       }
    
    
    @objc func writePost(_ sender: Any) {
        
        self.performSegue(withIdentifier: "newPost", sender: self)
       
        
    }
  
    
    
     @objc func profButtonPressed(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
     }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.data.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sectionCell", for: indexPath) as! sectionTableViewCell
        cell.senderButton.tag = indexPath.row
        cell.categoryLabel.text = self.data[indexPath.row]
        
        cell.senderButton.addTarget(self, action: #selector(sendView(sender:)), for: .touchUpInside)

        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "marketSB") as! marketTableViewController
        self.navigationController?.pushViewController(VC, animated: true)
        VC.myID = self.myID
        VC.myName = self.myName
        VC.navigationItem.title = self.data[indexPath.row]
    }

    
    @objc func sendView( sender: UIButton){
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "marketSB") as! marketTableViewController
        self.navigationController?.pushViewController(VC, animated: true)
        VC.myID = self.myID
        VC.myName = self.myName
        VC.navigationItem.title = self.data[sender.tag]
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
