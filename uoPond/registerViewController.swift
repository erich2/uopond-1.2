//
//  registerViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/19/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class registerViewController: UIViewController {
    
    @IBOutlet weak var usernameBox: UITextField!
    @IBOutlet weak var emailBox: UITextField!
    @IBOutlet weak var confirmEmailBox: UITextField!
    @IBOutlet weak var passwordBox: UITextField!
    @IBOutlet weak var confirmPasswordBox: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var toLoginButton: UIButton!
    @IBOutlet weak var registerSucc: UILabel!
    var userUid:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.view.addGestureRecognizer(tap)
        toLoginButton.layer.cornerRadius = 10
        registerButton.layer.cornerRadius = 10
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    @objc func tapped(sender: UIGestureRecognizer) {
           self.view.endEditing(true)
           print("tapped")
       }
    
    
    @IBAction func signUpAction(_ sender: Any) {
       
        if passwordBox.text != confirmPasswordBox.text {
            let alertController = UIAlertController(title: "Password Incorrect", message: "Please make sure your passwords match", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else if emailBox.text != confirmEmailBox.text{
            let alertController = UIAlertController(title: "Email Incorrect", message: "Please make sure your emails match", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            }
        else if passwordBox.text == nil || confirmPasswordBox.text == nil || emailBox.text == nil || confirmEmailBox.text == nil || usernameBox.text == nil{
            let alertController = UIAlertController(title: "Oops", message: "Please fill out all fields", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }else if emailBox.text!.contains("@uoregon.edu") || emailBox.text!.contains("the.peter.richards@gmail.com"){
            self.showSpinner(onView: self.view)
            Auth.auth().createUser(withEmail: emailBox.text!, password: passwordBox.text!) { (user, error) in
                if error == nil{
                    guard let uid = Auth.auth().currentUser?.uid else {
                        print("error in the uid ")
                        return
                    }
                    self.userUid = uid
                    print("BEFoRE the UID",self.userUid!)
                    let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/")
                    let userReference = ref.child("Users").child(uid)
                    let values = ["username" : self.usernameBox.text!, "email" : self.emailBox.text!]
                    userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                        if error != nil{
                            print(err!)
                            self.removeSpinner()
                            return
                        }else{
                            print("success!")
                            self.removeSpinner()
                            self.registerButton.isHidden = true
                            self.usernameBox.isHidden = true
                            self.emailBox.isHidden = true
                            self.confirmEmailBox.isHidden = true
                            self.passwordBox.isHidden = true
                            self.confirmPasswordBox.isHidden = true
                            self.registerSucc.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                self.performSegue(withIdentifier: "OneMoreStep", sender: self)
                            }
                            return
                        }
                    })
             
                }else{
                    self.removeSpinner()
                    let alertController = UIAlertController(title: "Password Incorrect", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        
        }else{
            let alertController = UIAlertController(title: "I'm sorry", message: "pond is only available to UO studetnts.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OneMoreStep"{
            let vc = segue.destination as! profileViewController
            vc.UID = self.userUid
        }
    }
    
    
    
    @IBAction func toLoginPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "toLogin", sender: self)
    }
    

    
}
var vSpinner: UIView?
extension UIViewController{
    func showSpinner(onView: UIView){
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        DispatchQueue.main.async{
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    
    func removeSpinner(){
        DispatchQueue.main.async{
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}

